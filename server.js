console.log ("Servidor proTechU");
//Se define la variable que implementara express
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
app.listen(port);
// Se define la variable para utilizar el bodyparse
var bodyparse = require('body-parser')
// Usamos esta setencia para analizar las aplicaciones que utilizan JSON
app.use(bodyparse.json())
// Se define variable para usar la herramienta requestJson
var requestJson = require('request-json')
// Se define una variable para el uso del path
//                           var path = require('path');
// Se dfine variable para el uso de json-query
var jsonQuery = require('json-query')
console.log("Escuchando en el puerto:" + port);



//Se utiliza la aPI de MLAB para la consulta de usuario, cliente, cuentas, movimientos y comercios
/*
//Lina api key BD
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumxlas/collections"
var apiKey = "apiKey=gAEct4cCHzdDKHJnqbdikj2F-JovLHR-"
*/

//PROTECHU APIS DEL PROYECTO
//Olalde
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumxjao/collections"
var apiKey = "apiKey=jsSFzJVEZL9GmrkKJBKuXYB0QQXyoumN"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)
//CLIENTE
//Lista todos los cliente
app.get('/v1/cliente/todos', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/cliente?f={}&' + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})
//Lista todos los usuarios
app.get('/v1/usuario/todos', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuario?f={}&' + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})
//Obtiene cuentas de un cliente en especifico introducido en el headers
app.get('/v1/cliente', function(req, res) {
  var idcliente = req.headers ['idcliente'];
  //var idcliente = req.params.idcliente
  var queryCliente = '/cliente?'+
    'f={"idcliente":0,"email":0,"celular":0, "cuentas.nip":0,"cuentas.movimientos":0}&'+
    'q={"idcliente":' + idcliente +'}&'+
    's={"tptarjeta":1}&'
  clienteMlab = requestJson.createClient(urlMlabRaiz + queryCliente + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})
//LOGIN
// Valida si ya es usuario
app.post('/v1/usuario/login', function(req, res) {
    var idusuario = req.headers ['email'];
    var password = req.headers ['password'];
    var queryLogin = "/usuario?" + 'q={"idusuario":"' + idusuario + '"}' + "&l=1&"
    clienteMlab = requestJson.createClient(urlMlabRaiz + queryLogin + apiKey)
    clienteMlab.get('', function(err, resM, body) {
        if (body.length == 1){
          if (body[0].logged){
            res.send('{"logged":"errorLOG"}')
          }
          //console.log("body 0:"+JSON.stringify(body[0]));
          if (body[0].password == password) {
            var verdadero = true
            var cambio = '{"$set":{"logged":'+ verdadero +'}}'
                //console.log(urlMlabRaiz+ queryLogin + apiKey + JSON.parse(cambio))
            clienteMlab.put(urlMlabRaiz+ queryLogin + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
                  res.send('{"logged":"'+verdadero+'","idcliente":"'+body[0].idcliente+'","idusuario":"'+body[0].idusuario+'","nombre":"'+body[0].nombre+'"}')
                })
           }
           else {
            res.send('{"logged":"errorPWD"}')
          }
        }
        else {
          res.send('{"logged":"errorUSR"}')
        }
    })
})
//Realiza el logout de la aplicacion
app.post('/v1/usuario/logout',function(req,res) {
  var idusuario = req.headers ['email'];
  var queryLogin = "/usuario?" + 'q={"idusuario":"' + idusuario + '"}' + "&l=1&"
  clienteMlab = requestJson.createClient(urlMlabRaiz + queryLogin + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (body.length == 1){
        if (body[0].logged) {
          var falso = false
          var cambio = '{"$set":{"logged":'+ falso +'}}'
          clienteMlab.put(urlMlabRaiz+ queryLogin + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
                res.send('{"logout":"'+falso+'","idcliente":"'+body[0].idcliente+'","idusuario":"'+body[0].idusuario+'","nombre":"'+body[0].nombre+'"}')
              })
         }
         else {
          res.send('{"logout":"errorLGN"}')
        }
      }
      else {
        res.send('{"logout":"errorUSR"}')
      }
  })
})

//Valida existencia de cliente para el registro
app.get('/v1/cliente/registro', function(req, res) {
  var email = req.headers ['email'];
  var plastico = req.headers ['plastico'];
  var nip = req.headers ['nip'];
  var queryCliente = '/cliente?'+
    'f={"idcliente":1,"nombre":1,"email":1}&'+
    'q={"email":"'+email+
       '","cuentas.plastico":"'+plastico+
       '","cuentas.nip":"'+nip+'"}&'+ "l=1&"
    's={}&'
  //console.log(urlMlabRaiz + queryCliente + apiKey);
  clienteMlab = requestJson.createClient(urlMlabRaiz + queryCliente + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    //console.log(body);
    if (body.length == 1){
      res.send(body)
    }
    else {
      res.send('{"registro":"errorCLI"}')
    }
  })
})
//Realiza el registro de cliente en la tabla de usuario para su acceso a la APP
app.post('/v1/usuario/registro', function(req, res) {
  var idcliente  = req.headers ['idcliente'];
  var idusuario  = req.headers ['email'];
  var nombre  = req.headers ['nombre'];
  var password  = req.headers ['password'];
  var logged  = false;
  var insertaUsuario = '{"idcliente":"'+idcliente+
                      '","idusuario":"'+idusuario+
                      '","nombre":"'+nombre+
                      '","password":"'+password+
                      '","logged":"'+logged+
                      '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuario?"  + apiKey)
  clienteMlab.post('', JSON.parse(insertaUsuario), function(err, resM, body) {
    res.send(body)
  })
})
//Trae los movimientos de una cuenta especifica
app.get('/v1/cliente/movimientos',function(req, res){
  var idcliente = req.headers ['idcliente'];
  var idcuenta = req.headers ['idcuenta'];
  var queryClienteCuenta = '/cliente?'+
    'f={"cuentas":1}&'+
    'q={"idcliente":'+idcliente+
       ',"cuentas.idcuenta":'+idcuenta+'}&'+
    's={}&'
  //console.log(urlMlabRaiz + queryClienteCuenta + apiKey)
  clienteMlab = requestJson.createClient(urlMlabRaiz + queryClienteCuenta + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      var cuentas = body[0].cuentas;
      for (var cta in cuentas){
        if (cuentas[cta].idcuenta == idcuenta) {
          //console.log("cuentas:"+JSON.stringify(cuentas[cta]))
          var movimientos =cuentas[cta].movimientos;
          //items = [ {id:1, value:3, perc:0.5}, {id:2, value:2, perc:0.3}, {id:3, value:1, perc:0.2} ]
          movimientos.sort(function (a, b){
              return (b.fecha - a.fecha)
          })
          res.send(movimientos)
        }
      }
    }
    else {
      res.send("err")
    }
  })
});



//FIN PROTECHU APIS DEL PROYECTO
//FIN PROTECHU APIS DEL PROYECTO
